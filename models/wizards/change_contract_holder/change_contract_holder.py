from trytond.wizard import Wizard, StateAction, StateView, StateTransition
from trytond.wizard import Button

from ...services.change_contract_holder import ChangeContractHolderService
from ...activities.activity_from_holder_contract_change import ActivityFromHolderContractChange


class ChangeContractHolder(Wizard):
    """ Change Contract Holder """

    __name__ = 'change.contract.holder'

    start = StateView(
        'change.contract.holder.start',
        'contract_changes_wizards_somconnexio.change_contract_holder_start',
        [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Continue', 'check_data', 'tryton-ok', default=True)
        ]
    )
    check_data = StateView(
        'change.contract.holder.check_data',
        'contract_changes_wizards_somconnexio.change_contract_holder_check_data',
        [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Confirm', 'change_contract_holder', 'tryton-ok', default=True),
        ]
    )
    view_contract = StateAction('contract.act_contract')
    change_contract_holder = StateTransition()

    def default_check_data(self, fields):
        return {
            'contract': self.start.contract.id,
            'party': self.start.party.id,
            'end_date': self.start.end_date,
            'description': self.start.description,
        }

    def do_view_contract(self, action):
        data = {
            'res_id': self.new_contract.id,
        }
        return action, data

    def transition_change_contract_holder(self):
        self.new_contract = ChangeContractHolderService(self.check_data).run()

        ActivityFromHolderContractChange(
            self.check_data.contract,
            self.check_data.description,
            self.check_data.party.rec_name
        ).create()

        return 'view_contract'
