from otrs_somconnexio.otrs_models.coverage.adsl import ADSLCoverage
from otrs_somconnexio.otrs_models.coverage.mm_fibre import MMFibreCoverage
from otrs_somconnexio.otrs_models.coverage.vdf_fibre import VdfFibreCoverage
from otrs_somconnexio.otrs_models.providers import Providers

from trytond.model import ModelView, fields
from trytond.pool import Pool
from trytond.pyson import And, Equal, Eval, Or


_CONTRACT_SERVICE = [
    ('mobile', 'Mobile'),
    ('adsl', 'ADSL or Fibre'),
    ('mobile_adsl', 'Mobile + ADSL or Fibre'),
    ('do_partner', 'Do a Partner'),  # do_partner is last option
]

_CONTRACT_INTERNET_NOW_SELECTOR = [
    ('adsl', 'ADSL'),
    ('fibre', 'Fibre'),
]

_CONTRACT_INTERNET_PHONE_SELECTOR = [
    ('phone', 'Phone'),
    ('no_phone', 'No Phone'),
    (None, '')
]

_CONTRACT_INTERNET_SPEED_SELECTOR = [
    ('60MB', '100 Mb'),
    ('120MB', '600 Mb'),
    ('400MB', '1 Gb'),
    (None, '')
]

_CONTRACT_INTERNET_OPTIONS_SELECTOR = [
    ('adsl', 'ADSL'),
    ('fibre', 'Fibre'),
]

_CONTRACT_INTERNET_PHONE_MINUTES_SELECTOR = [
    ('100+', '100 MIN Phone + Mobile'),
    ('100', '1000 MIN Phone'),
    (None, '')
]


class ChangeContractAddressStart(ModelView):
    """ Change Contract Address start """

    __name__ = 'change.contract.address.start'

    party = fields.Many2One('party.party', 'Party', required=True)
    service = fields.Selection(
        _CONTRACT_SERVICE,
        'Service',
        states={
            'readonly': True,
            'required': True,
            'invisible': True
        },
        depends=['party']
    )
    internet_now = fields.Selection(
        _CONTRACT_INTERNET_NOW_SELECTOR,
        'What do you have now',
        states={
            'required': True,
            'invisible': ~Eval('party')
        },
    )
    internet_phone = fields.Selection(
        _CONTRACT_INTERNET_PHONE_SELECTOR,
        'Internet Phone',
        states={
            'required': Equal(Eval('internet_contract'), 'adsl'),
            'invisible': Or(
                ~Eval('party'),
                ~Equal(Eval('internet_contract'), 'adsl')
            )
        },
    )
    internet_phone_minutes = fields.Selection(
        _CONTRACT_INTERNET_PHONE_MINUTES_SELECTOR,
        'Internet Phone Minutes',
        states={
            'required': And(
                Equal(Eval('internet_contract'), 'adsl'),
                Equal(Eval('internet_phone'), 'phone')
            ),
            'invisible': Or(
                ~Eval('party'),
                ~Equal(Eval('internet_contract'), 'adsl'),
                ~Equal(Eval('internet_phone'), 'phone')
            )
        },
    )
    internet_speed = fields.Selection(
        _CONTRACT_INTERNET_SPEED_SELECTOR,
        'Internet Speed',
        states={
            'required': Equal(Eval('internet_contract'), 'fibre'),
            'invisible': Or(
                ~Eval('party'),
                ~Equal(Eval('internet_contract'), 'fibre'),
            )
        },
    )
    internet_contract = fields.Selection(
        _CONTRACT_INTERNET_OPTIONS_SELECTOR,
        'What do you like',
        states={
            'required': True,
            'invisible': ~Eval('party')
        },
    )
    internet_city = fields.Char(
        'Internet City',
        states={
            'required': True,
            'invisible': ~Eval('party')
        },
    )
    internet_street = fields.Char(
        'Internet Street',
        states={
            'required': True,
            'invisible': ~Eval('party')
        },

    )
    internet_zip = fields.Char(
        'Internet Zip',
        states={
            'required': True,
            'invisible': ~Eval('party')
        },

    )
    internet_country = fields.Many2One(
        'country.country',
        'Internet Country',
        states={
            'required': True,
            'invisible': ~Eval('party')
        },
    )
    internet_subdivision = fields.Many2One(
        'country.subdivision',
        'Internet Subdivision',
        states={
            'required': True,
            'invisible': ~Eval('party')
        },
    )
    provider = fields.Selection(
        Providers.VALUES,
        'Provider',
        states={
            'required': True,
            'invisible': ~Eval('party')
        },
    )
    receivable_bank_account = fields.Many2One(
        'bank.account',
        'Receivable Bank Account',
        states={
            'required': True,
            'invisible': ~Eval('party')
        },
        domain=[
            ('owners', '=', Eval('party'))],
        required=True,
        help='Party bank account'
    )
    adsl_coverage = fields.Selection(
        ADSLCoverage.VALUES,
        'ADSL Coverage',
        states={
            # Required only if the change if from Fiber to ADSL
            'required': Equal(Eval('internet_contract'), 'adsl'),
            # Invisible only if the change if from ADSL to ADSL or from ADSL/Fiber to Fiber
            'invisible': Or(
                ~Eval('party'),
                Equal(Eval('internet_contract'), 'fibre'),
            )
        },
    )
    mm_fiber_coverage = fields.Selection(
        MMFibreCoverage.VALUES,
        'MM Fiber Coverage',
        states={
            # Required only if the change is to Fiber
            'required': And(
                Equal(Eval('internet_contract'), 'fibre'),
                Equal(Eval('internet_speed'), '60MB'),
            ),
            # Invisible only if the change is to ADSL
            'invisible': Or(
                ~Eval('party'),
                ~Equal(Eval('internet_speed'), '60MB')
            )
        },
    )
    vdf_fiber_coverage = fields.Selection(
        VdfFibreCoverage.VALUES,
        'Vdf Fiber Coverage',
        states={
            # Required only if the change is to Fiber
            'required': Equal(Eval('internet_contract'), 'fibre'),
            # Invisible only if the change is to ADSL
            'invisible': ~Eval('party')
        },
    )
    notes = fields.Char(
        'Notes',
        states={
            'invisible': ~Eval('party')
        })

    @staticmethod
    def default_service():
        return 'adsl'

    @staticmethod
    def default_internet_country():
        country = Pool().get('country.country').search(
            ('code', '=', "ES")
        )
        return country[0].id
