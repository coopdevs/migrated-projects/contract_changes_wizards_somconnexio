from trytond.model import ModelView, fields
from trytond.pyson import Eval


class ChangeContractEmailStart(ModelView):
    """ Change Contract Email start """

    __name__ = 'change.contract.email.start'

    party = fields.Many2One('party.party', 'Party', required=True)
    email = fields.Many2One(
        'party.contact_mechanism',
        'Contact email',
        required=True,
        depends=['party'],
        domain=[('type', '=', 'email'), ('party', '=', Eval('party'))]
    )
    description = fields.Char("Description", required=True)
