from .activity_from_contract_change import ActivityFromContractChange


class ActivityFromHolderContractChange(ActivityFromContractChange):
    ACTIVITY_TYPE_NAME = 'Canvi de Titular'
    SUBJECT_MESSAGE = u'Holder change: {}'
