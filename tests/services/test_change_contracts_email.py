from mock import Mock, patch
import unittest2 as unittest

from models.services.change_contracts_email import ChangeContractsEmailService


class ChangeContractsEmailServiceTestCase(unittest.TestCase):

    @patch('models.services.change_contracts_email.Pool', return_value=Mock(spec=['get']))
    def test_run(self, MockPool):
        email = object()
        contracts = [Mock(spec=['contact_email'])]

        MockContract = Mock(spec=['write'])

        def pool_get_side_effect(model):
            if model == 'contract':
                return MockContract
        MockPool.get.side_effect = pool_get_side_effect

        ChangeContractsEmailService(email, contracts).run()

        for contract in contracts:
            self.assertEqual(
                contract.contact_email,
                email
            )

        MockContract.write.asset_called_once_with(
            contracts,
            {'contact_email': email}
        )
