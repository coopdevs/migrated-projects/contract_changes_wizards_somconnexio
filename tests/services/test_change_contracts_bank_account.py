from mock import Mock, patch
import unittest2 as unittest

from models.services.change_contract_bank_account import ChangeContractsBankAccountService


class ChangeContractsBankAccountServiceTestCase(unittest.TestCase):

    @patch('models.services.change_contract_bank_account.Pool', return_value=Mock(spec=['get']))
    def test_run(self, MockPool):
        bank_account = 123
        contracts = [Mock(spec=['receivable_bank_account'])]

        MockContract = Mock(spec=['write'])

        def pool_get_side_effect(model):
            if model == 'contract':
                return MockContract
        MockPool.get.side_effect = pool_get_side_effect

        ChangeContractsBankAccountService(bank_account, contracts).run()

        for contract in contracts:
            self.assertEqual(
                contract.receivable_bank_account,
                bank_account
            )

        MockContract.write.asset_called_once_with(
            contracts,
            {'receivable_bank_account': bank_account}
        )
