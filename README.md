# Tryton 3.8ish[1] module do changes in Contracts

This is an ad-hoc tryton module to manage the changes in the Contracts.
this module is related to the [`opencell_somconnexio`](https://gitlab.com/coopdevs/opencell_somconnexio_tryton) module. It implements a wizard to manage the following contract changes:

* Change holder
* Change email
* Change IBAN (bank account)
* Change address

All these changes are special because are also related to the OpenCell integration and we need to propagate them to OpenCell.
This module only changes the local data (ERP) but we have observer methods in `opencell_somconnexio` that send the changes to OpenCell.

[1] The version of tryton 3.8 patched by NanTIC that we use.

## Python version

We are using [Pyenv](https://github.com/pyenv/pyenv) to fix the Python version and the virtualenv to test the package.

You need:

* Install and configure [`pyenv`](https://github.com/pyenv/pyenv)
* Install and configure [`pyenv-virtualenvwrapper`](https://github.com/pyenv/pyenv-virtualenvwrapper)
* Install locally the version of python needed:

```
$ pyenv install 2.7.9
```

* Create the virtualenv to use:

```
$ pyenv virtualenv 2.7.9 contract_changes_wizards_somconnexio
```
